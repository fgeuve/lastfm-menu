const express = require('express');
const app = express();
const path = require('path');
const globalConfig = require(path.resolve('./config/globalConfig.json'));

const port = globalConfig.app.port;

// Iniciar el servidor
app.listen(port, () => {
  console.log(`Servidor escuchando en el puerto ${port}`);
});
